import React, { Component } from 'react'

import { translate } from '../helper'

const LANGUAGES = [
  { name: 'Java', exp: 6, proficiency: 7 },
  { name: 'JavaScript', exp: 6, proficiency: 8 },
  // {name: 'ActionScript', exp: 3, proficiency: },
  { name: 'PHP', exp: 3, proficiency: 5 },
  { name: 'Golang', exp: 3, proficiency: 6 },
  { name: 'Python', exp: 2, proficiency: 4 },
  { name: 'Ruby', exp: 1, proficiency: 3 }
]

const FRONTEND = [
  { name: 'ReactJS', exp: 2, proficiency: 8 },
  { name: 'AngularJS', exp: 3, proficiency: 7 }
]

const MIDDLEWARE = [
  {
    name: 'Websphere Process Server / Enterprise Service Bus',
    exp: 2,
    proficiency: 7
  },
  { name: 'Custom Middleware', exp: 3, proficiency: 7 }
  // {name: 'Websphere Application Server', exp: 2, proficiency: 5},
  // {name: 'Oracle Weblogic Server', exp: 1, proficiency: 3}
]

const DATABASES = [
  { name: 'Posgres', exp: 3, proficiency: 6 },
  { name: 'Oracle', exp: 3, proficiency: 6 },
  { name: 'DB2', exp: 2, proficiency: 4 },
  { name: 'MySQL', exp: 2, proficiency: 4 }
]

export default class Technologies extends Component {
  render() {
    // const lang = this.props.location.pathname.split('/')[1]
    const lang = this.props.language
    return (
      <section id="tech" className="section tech">
        <div className="section-content tech-content">
          <Technology
            title={translate(
              {
                titleDE: 'Programmiersprachen',
                titleEN: 'Programming languages'
              },
              'title',
              lang
            )}
            entries={LANGUAGES}
          />
          <Technology
            title={translate(
              { titleDE: 'Frontend', titleEN: 'Frontend' },
              'title',
              lang
            )}
            entries={FRONTEND}
          />
          <Technology
            title={translate(
              { titleDE: 'Middleware', titleEN: 'Middleware' },
              'title',
              lang
            )}
            entries={MIDDLEWARE}
          />
          <Technology
            title={translate(
              { titleDE: 'Datenbanken', titleEN: 'Databases' },
              'title',
              lang
            )}
            entries={DATABASES}
          />
        </div>
      </section>
    )
  }
}

class Technology extends Component {
  render() {
    return (
      <div>
        <h3 className="tech-content-title">{this.props.title}</h3>
        {this.props.entries.map((entry, i) => {
          return (
            <div key={i} className="tech-content-entry content-entry-tile">
              <div className="tech-content-entry-name">{entry.name}</div>
              <div
                className={
                  'tech-content-entry-exp tech-content-entry-exp--' +
                  entry.exp +
                  ' tech-content-entry-prof--' +
                  entry.proficiency
                }
              >
                <span>{entry.proficiency}</span>
                {/* {new Date().getFullYear() - entry.exp + (lang === 'de' ? 'Jahre' : 'years')} */}
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}

import React, { Component } from 'react'

const CV_ENTRIES = [
  {year: '2016', company: 'Merisier', website: 'https://merisier.de', work: 'Web Development', frontend: 'AngularJS, ReactJS', backend: 'Golang, Python', db: 'Postgres'},
  {year: '2016', company: 'Tamedia', website: 'https://tamedia.ch', work: 'Data Engineering', frontend: 'Grafana', backend: 'Ruby', db: 'Influx DB'},
  {year: '2016-20??', company: 'Katholische Akademie', website: 'http://erinnerte-zukunft.de', work: 'Web Development', frontend: 'ReactJS', backend: '', db: ''},
// {nameDE: 'Educational Engineering Lab, Institut für Informatik, Universität Zürich', nameEN: 'Educational Engineering Lab, Department of Informatics, University of Zurich', work: 'Game Programming', technologies: 'Flash, Actionscript, Coldfusion', descriptionDE: '', descriptionEN: ''},
// {nameDE: 'Trialox AG', nameEN: 'Trialox AG', work: 'Software Development', technologies: 'Flash, Actionscript, Coldfusion', descriptionDE: '', descriptionEN: ''}

]

export default class CV extends Component {
  render () {
    return <section id='cv' className='cv'>
             <div className='cv-content'>
               {CV_ENTRIES.map((entry, i) => {
                  return <div className='cv-content-entry' key={i}>
                           <div className='content-entry--3of12'>
                             {entry.year}
                           </div>
                           <div className='content-entry--5of12'>
                             {entry.company}
                           </div>
                           <div className='content-entry--4of12'>
                             {entry.work}
                           </div>
                           {/*<div className='content-entry--2of12'>
                                                                                                                                                                                                                                                                                                                                 {entry.frontend}
                                                                                                                                                                                                                                                                                                                               </div>
                                                                                                                                                                                                                                                                                                                               <div className='content-entry--2of12'>
                                                                                                                                                                                                                                                                                                                                 {entry.backend}
                                                                                                                                                                                                                                                                                                                               </div>
                                                                                                                                                                                                                                                                                                                               <div className='content-entry--2of12'>
                                                                                                                                                                                                                                                                                                                                 {entry.db}
                                                                                                                                                                                                                                                                                                                               </div>*/}
                         </div>
                })}
             </div>
           </section>
  }
}

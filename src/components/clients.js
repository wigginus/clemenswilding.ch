import React, { Component } from 'react'

const CLIENTS = [
  {name: 'Educational Engineering Lab', website: 'http://www.ifi.uzh.ch/ee', storyEN: 'My first professional experience was working for the EE lab at the university of Zurich. The task was to further develop a project management game. I first did a three month long intership and then I ', storyDE: '', jobType: 'Internship', technologies: ['ActionScript / Flash', 'Coldfusion', 'MS SQL'], skills: ['Leadership', 'Programming' ]},
  {name: 'Trialox AG', website: '', logo: '', technologies: ['Java SE', 'Semantic Web', 'Java Security'], skills: ['Research']},
  {name: 'eCF - Get involved in Corporate Finance', website: 'http://www.getinvolved.uzh.ch/', logo: '', technologies: ['PHP', 'MySQL', 'JavaScript'], skills: ['Interdisciplinary', 'Support']},
  {name: 'ipt - innovation process technology', website: 'http://ipt.ch/', logo: '', technologies: ['Web Services', 'Business Processes', 'TODO'], skills: ['']},
  {name: 'Federal Office of Information Technology, Systems and Telecommunication', website: 'http://www.bit.admin.ch', logo: '', technologies: ['Java EE', 'Java Server Faces', 'Oracle DB'], skills: ['Pogramming', 'Architecture']},
  {name: 'SwissRe', website: 'http://www.swissre.com/', logo: '', technologies: ['IBM Websphere Process Server', 'Java EE', 'IBM DB2 / Oracle DB'], skills: ['Pogramming', 'Architecture', 'Support']},
  {name: 'Merisier', website: 'https://merisier.de/', logo: '', technologies: ['Google Go', 'AngularJs', 'ReactJs'], skills: ['Programming', 'Design']},
  {name: 'Katholische Akademie Berlin', website: 'http://katholische-akademie-berlin.de/', logo: '', technologies: ['ReactJs', 'Webpack'], skills: ['Programming']},
  {name: 'Tamedia', website: 'http://www.tamedia.ch', logo: '', technologies: ['Ruby', 'Java / Android', 'InfluxDB'], skills: ['Programming', 'Research']}
]

export default class Clients extends Component {
  render () {
    return <section id='clients' className='clients'>
             <div className='clients-content'>
               {CLIENTS.map((entry, i) => {
                  return <div key={i} className='clients-content-entry'>
                           <div className='clients-content-entry-techs'>
                             {entry.technologies.map((tech, j) => {
                                return <span key={j} className='clients-content-entry-techs-tech'>{tech}</span>
                              }
                              )}
                           </div>
                           <div className='clients-content-entry-skills'>
                             {entry.skills.map((tech, j) => {
                                return <span key={j} className='clients-content-entry-skills-skill'>{tech}</span>
                              }
                              )}
                           </div>
                         </div>
                })}
             </div>
           </section>
  }
}

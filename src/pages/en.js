import React, { Component } from 'react'

import Helmet from '../components/helmet'
import Menu from '../components/menu'
import Landing from '../components/landing'
// import CV from '../components/cv'
import Contact from '../components/contact'
// import Clients from '../components/clients'
// import Technologies from '../components/technologies'
import Skills from '../components/skills'

import '../css/App.css'

export default class OnePagerEn extends Component {
  render() {
    return (
      <>
        <Helmet language="en" />
        <Menu language="en" />
        <Landing language="en" />
        {/* <Clients {...this.props} /> */}
        <Skills language="en" />
        <Contact language="en" />
        {/* <Technologies {...this.props} /> */}
      </>
    )
  }
}

module.exports = {
  siteMetadata: {
    title: `Clemens Wilding`,
    siteUrl: `https://clemenswilding.ch`,
    description: `Clemens Wilding is a software engineer and consultant in Berlin.`
  },
  plugins: [`gatsby-plugin-react-helmet`]
}
